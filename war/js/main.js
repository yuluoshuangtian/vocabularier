var userId;
function tableFunctions(){
	//make the "X" button in the table red when hovering
	$(".table-remove").hover(function(){
		$(this).parent().css("color","red");
	}, function(){
		$(this).parent().css("color","");
	});
	//"X" button's function
	$(".table-remove").click(function(){
		var status = "remove";
		var word = $(this).parent().parent().find(".tableWord").text();
		var sentence = $(this).parent().parent().find(".tableSentence").text();
		$.post("vocabularier", {
			status : status,
			saveWord : word,
			saveSentence : sentence,
			userId: userId
		}, function(data) {
			var alertDiv = document.createElement("DIV");
			alertDiv.className = "alert alert-warning";
			var text = document.createTextNode(data + "deleted.");
			alertDiv.appendChild(text);
			document.getElementById("success-alert").appendChild(
						alertDiv);
			$(alertDiv).fadeTo(2000, 5).slideUp(
						500,
					function() {
						document.getElementById("success-alert")
									.removeChild(alertDiv);
					});
			});
		$(this).parent().parent().remove();
	});
	
	//mark the row after clicked
	$(".tableRow").click(function(){
		$(".tableRow").css('background-color', '');
		$(".tableRow").attr("id","");
		$(this).css('background-color', 'lightblue');
		$(this).attr("id","rowMarked");
		$("#edit-word").val($(this).find(".tableWord").text());
		$("#edit-sentence").val($(this).find(".tableSentence").text());
	});
};

$(document).ready(function() {	
	//get user's Id after login
	$.get("GetUserIdServlet", function(data) {
		userId = data;
		document.getElementById("userIdButton").innerHTML = data + "<span class='caret'></span>";
	});
	//show the user's vocabulary
	$("#showMyVocab").click(function(e) {
		$.post("ShowVocabulary", {
			userId : userId
		}, function(data) {
			var table = document.getElementById("vocabularyTable");
			
			//refresh the table content
			$("#vocabularyTable").find("tr:gt(0)").remove();
			
			var vocabulary = data;
			//create row, cell element 
			for (var i = 0; i < vocabulary.length; i++) {
				var row = table.insertRow(-1);
			    var wordCell = document.createTextNode(vocabulary[i].word);
			    var sentenceCell = document.createTextNode(vocabulary[i].sentence);
			    var removeCell = document.createElement("SPAN");
			    			    			    
			    var tdWord = document.createElement('td');
			    var tdSentence = document.createElement('td');
			    var tdRemove = document.createElement('td');
			    			    
		        row.appendChild(tdWord);
		        row.appendChild(tdSentence);
		        row.appendChild(tdRemove);
		         
				row.cells[0].appendChild(wordCell);
				row.cells[1].appendChild(sentenceCell);
				row.cells[2].appendChild(removeCell);
				
				row.className = "tableRow";
				removeCell.className = "table-remove glyphicon glyphicon-remove";
				tdWord.className = "tableWord";
			    tdSentence.className = "tableSentence";
				tdRemove.className = "tableRemove";
				$(tdRemove).css("font-size","17px");
			}
			tableFunctions();			
		})
	})
	//edit chosen items
	$("#edit-save-button").click(function(){
		var word = $("#edit-word").val();
		var sentence = $("#edit-sentence").val();
		var originalWord = $("#rowMarked").find(".tableWord").text();
		var originalSentence = $("#rowMarked").find(".tableSentence").text();
		
		$.post("vocabularier", {
			status : "remove",
			saveWord : originalWord,
			saveSentence : originalSentence,
			userId: userId
		}, function(data) {});
		$.post("vocabularier", {
			status : "add",
			saveWord : word,
			saveSentence : sentence,
			userId: userId
		}, function(data) {
			var alertDiv = document.createElement("DIV");
			alertDiv.className = "alert alert-info";
			var text = document.createTextNode(data + "updated.");
			alertDiv.appendChild(text);
			document.getElementById("success-alert").appendChild(
						alertDiv);
			$(alertDiv).fadeTo(2000, 5).slideUp(
						500,
					function() {
						document.getElementById("success-alert")
									.removeChild(alertDiv);
					});
			});	
		$("#rowMarked").find(".tableWord").text(word);
		$("#rowMarked").find(".tableSentence").text(sentence);
		$("#edit-word").val("");
		$("#edit-sentence").val("");
		$("#editModal").modal('hide');
	});
	
	//add item to the vocabulary	
	$("#add-save-button").click(function(){		
		var word = $("#add-word").val();
		var sentence = $("#add-sentence").val();
		if (word != ""){
			var status = "add";
			$.post("vocabularier", {
				status : status,
				saveWord : word,
				saveSentence : sentence,
				userId: userId
			}, function(data) {
				var alertDiv = document.createElement("DIV");
				alertDiv.className = "alert alert-success";
				var text = document.createTextNode(data + "saved.");
				alertDiv.appendChild(text);
				document.getElementById("success-alert").appendChild(
							alertDiv);
				$(alertDiv).fadeTo(2000, 5).slideUp(
							500,
						function() {
							document.getElementById("success-alert")
										.removeChild(alertDiv);
						});
				});			
			var table = document.getElementById("vocabularyTable");
			var row = table.insertRow(-1);
			var wordCell = document.createTextNode(word);
		    var sentenceCell = document.createTextNode(sentence);
		    var removeCell = document.createElement("SPAN");
		    			    			    
		    var tdWord = document.createElement('td');
		    var tdSentence = document.createElement('td');
		    var tdRemove = document.createElement('td');
		    			    
	        row.appendChild(tdWord);
	        row.appendChild(tdSentence);
	        row.appendChild(tdRemove);
	         
			row.cells[0].appendChild(wordCell);
			row.cells[1].appendChild(sentenceCell);
			row.cells[2].appendChild(removeCell);
			
			row.className = "tableRow";
			removeCell.className = "table-remove glyphicon glyphicon-remove";
			tdWord.className = "tableWord";
		    tdSentence.className = "tableSentence";
			tdRemove.className = "tableRemove";
			$(tdRemove).css("font-size","17px");
			
			tableFunctions();
			$("#add-word").val("");
			$("#add-sentence").val("");
			$("#addModal").modal('hide');
		}		
	})	
});

function myFunction() {
	var textToBeEdited, textToBeShowed;
	var word, sentence, status;
	var element;
	
	// pass the text from input area to output area
	textToBeEdited = document.getElementById("textToBeEdited").value;
	textToBeShowed = textToBeEdited;
	document.getElementById("textToBeShowed").innerHTML = textToBeShowed;
	
	$("#textToBeShowed")
			.each(
					function() {
						var $this = $(this);
						$this
								.html($this
										.text()
										// add <br> after line ending
										.replace(/(\r?\n)/g, "<br>")
										// surround the word with <span> and add
										// pop over confirmation
										.replace(/([(\w)åäöÅÄÖ]+)/g,
												"<span class=word data-toggle=confirmation>$1</span>")
										// surround the sentence with <span>
										.replace(
												/(((?![.!?]['"]?\s).)*[.!?]?['"]?)(\s|$)/g,
												"<span class=sentence>$1</span>")
										// remove <span> tag that surrounding
										// with <br>
										.replace(
												/<span class=word data-toggle=confirmation>br<\/span>/g,
												"br><br></span><span class=sentence"));
					});

	// If the word was added to the database, it will be marked yellow, rgb(255,
	// 255, 0)
	// Else, the word will be marked red rgb(255, 0, 0) when hovering, and
	// change back to normal thereafter.

	$('.word').hover(function() {
		if ($(this).css('background-color') != 'rgb(255, 255, 0)') {
			$(this).css('background-color', 'red');
		}
	}, function() {
		if ($(this).css('background-color') == 'rgb(255, 0, 0)') {
			$(this).css('background-color', '');
		}
	});

	// If the word is marked yellow, means it was added to the database, click
	// to remove it, pop up alert, then mark the word back to normal.
	// Else, click to add it to database, and marked it yellow, pop up alert.
	$(".word").click(
			function() {
				if ($(this).css('background-color') == 'rgb(255, 255, 0)') {
					status = "remove";
					word = $(this).text();
					sentence = $(this).parent().text();
					$.post("vocabularier", {
						status : status,
						saveWord : word,
						saveSentence : sentence,
						userId: userId
					}, function(data) {
						var alertDiv = document.createElement("DIV");
						alertDiv.className = "alert alert-warning";
						var text = document.createTextNode(data + "deleted.");
						alertDiv.appendChild(text);
						document.getElementById("success-alert").appendChild(
								alertDiv);
						$(alertDiv).fadeTo(2000, 5).slideUp(
								500,
								function() {
									document.getElementById("success-alert")
											.removeChild(alertDiv);
								})
					});
					$(this).css('background-color', '');
				} else {
					$(this).css('background-color', 'yellow');
					status = "add";
					word = $(this).text();
					sentence = $(this).parent().text();
					$.post("vocabularier", {
						status : status,
						saveWord : word,
						saveSentence : sentence,
						userId: userId
					}, function(data) {
						var alertDiv = document.createElement("DIV");
						alertDiv.className = "alert alert-success";
						var text = document.createTextNode(data + "saved.");
						alertDiv.appendChild(text);
						document.getElementById("success-alert").appendChild(
								alertDiv);
						$(alertDiv).fadeTo(2000, 5).slideUp(
								500,
								function() {
									document.getElementById("success-alert")
											.removeChild(alertDiv);
								});
					});
				}
			});

	$("[data-toggle='confirmation']").popConfirm({
		title : "Add or delete the word",
		content : "Are you sure?",
		placement : "bottom" // (top, right, bottom, left)
	});

	// sentence will be marked lightblue when hovering, and then changed back to
	// default
	$('.sentence').hover(function() {
		$(this).css('background-color', 'lightblue');
	}, function() {
		$(this).css('background-color', '');
	});
};

function clearContent() {
	document.getElementById("textToBeEdited").value = "";
	document.getElementById("textToBeShowed").innerHTML = "";
};