package com.yuszh.vocabularier.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("static-access")
	@Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        UserService userService = UserServiceFactory.getUserService();

        String thisURL = req.getRequestURI();

        resp.setContentType("text/html");
        String loginURL = userService.createLoginURL(thisURL);

        if (req.getUserPrincipal() != null) {
        	resp.setStatus(resp.SC_MOVED_TEMPORARILY);
            resp.setHeader("Location", "index.html");

        } else {
        	resp.setStatus(resp.SC_MOVED_TEMPORARILY);
            resp.setHeader("Location", loginURL);
        }        
    }
}