package com.yuszh.vocabularier.controller;

import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetUserIdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        resp.setContentType("text/html");
        if (req.getUserPrincipal() != null) {
            resp.getWriter().println(req.getUserPrincipal().getName());
        } else {
        	resp.getWriter().println("error");
        }
        
    }
}
