package com.yuszh.vocabularier.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.*;

import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;
import com.yuszh.vocabularier.model.Dao;
import com.yuszh.vocabularier.model.VocabularyModel;

@SuppressWarnings("serial")
public class ShowVocabularyServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		String userId = req.getParameter("userId");
		resp.setContentType("application/json");
		JSONArray vocabulary = new JSONArray();
		JSONObject item;
		List<VocabularyModel> vocabularyList = Dao.INSTANCE.getVocabularyList(userId);
		try {
			for (VocabularyModel vm : vocabularyList) {
				String word = vm.getWord();
				String sentence = vm.getSentence();
				item = new JSONObject();
				item.put("word", word);
				item.put("sentence", sentence);
				vocabulary.put(item);
			}
			resp.getWriter().print(vocabulary.toString());
		} catch (JSONException e) {
			// error handling
		}
	}
}
