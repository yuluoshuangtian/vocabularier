package com.yuszh.vocabularier.controller;

import java.io.IOException;
import javax.servlet.http.*;

import com.yuszh.vocabularier.model.Dao;

@SuppressWarnings("serial")
public class VocabularierServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String word = req.getParameter("saveWord");
		String sentence = req.getParameter("saveSentence");
		String status = req.getParameter("status");
		String userId = req.getParameter("userId");
		
		Dao.INSTANCE.editVocabulary(word, sentence, userId, status);
		
		resp.getWriter().print(" The word '" + word + "' has been ");		
	}
}
