package com.yuszh.vocabularier.model;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.List;

public enum Dao {
	INSTANCE;
	
	public void editVocabulary(String word, String sentence, String userId, String status){
		//if the status is "add", then add new item in the vocabulary
		if ("add".equals(status)){
			VocabularyModel item = new VocabularyModel(word, sentence, userId);
			ofy().save().entity(item).now();
		//if the status is "remove", then remove all the items that has the same word and sentence
		}else if ("remove".equals(status)){
			List<VocabularyModel> items = ofy().load().type(VocabularyModel.class)
					.filter("userId", userId)
					.filter("word", word)
					.filter("sentence", sentence)					
					.list();
			for (VocabularyModel item : items){
				ofy().delete().entity(item).now();
			}			
		}
	}
	
	public List<VocabularyModel> getVocabularyList(String userId){
		return ofy().load().type(VocabularyModel.class)
				.filter("userId", userId)
				.list();
	}
}
