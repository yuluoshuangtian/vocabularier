package com.yuszh.vocabularier.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class VocabularyModel {
  @Id private Long id;
  @Index private String word;
  @Index private String sentence;
  @Index private String userId;
  
  public VocabularyModel(){
	  
  }
    
  public VocabularyModel(String word, String sentence, String userId){
	  this();
	  this.word = word;
	  this.sentence = sentence;
	  this.userId = userId;
  }
  
  public long getId(){
	  return id;
  }
   
  public String getWord(){
	  return word;
  }
  
  public String getSentence(){
	  return sentence;
  } 
  
  public String getUerID(){
	  return userId;
  }
  
  public void setWord(String newWord){
	  word = newWord;
  }
  public void setSentence(String newSentence){
	  sentence = newSentence;
  }
 
}

